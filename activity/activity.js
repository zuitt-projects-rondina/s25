db.fruits.aggregate([
				{
					$match : {"supplier" : "Red Farms Inc."}
				},
				{
					$count : "itemsOnRedFarm"
				}

			])


db.fruits.aggregate([
				{
					$match : {"price" : {$gt : 50}}
				},
				{
					$count : "noOfItemsGreaterThan50"
				}

			])

db.fruits.aggregate([
			{
				$match : {"onSale":true}
			},
			{
				$group : {_id : "$supplier", avgPrice : {$avg : "$price"} }
			}


	])

db.fruits.aggregate([
			{
				$match : {"onSale":true}
			},
			{
				$group : {_id : "$supplier", maxPricePerSupplier : {$max : "$price"} }
			}


	])

db.fruits.aggregate([
			{
				$match : {"onSale":true}
			},
			{
				$group : {_id : "$supplier", minPricePerSupplier : {$min : "$price"} }
			}


	])







